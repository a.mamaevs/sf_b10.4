server{
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    #имя сервера:
    #server_name _;
   
    # корневая директория
    root {{ app_dir }}/default;
        
    location ~ \.php$ {
        try_files $uri = 404;
        include fastcgi_params;
        fastcgi_pass  unix:/var/run/php/php{{php_version}}-fpm.sock;
        fastcgi_index index.php;

        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    }
    
    # порядок индексов    
    location /
    {
        index  index.php index.html index.htm;
    }        
}

